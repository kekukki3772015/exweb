<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>
<body>
	<a href="list">Listi vaade</a>
	<br/>
	<br/>
	<c:if test="${not empty errors}">
		<c:forEach var="error" items="${errors}">
			<c:out value="${error}"></c:out><br/>
		</c:forEach>
	</c:if>
	<form method="post" action="add" >
		Nimi: <input name="name"/><br/>
		Vanus: <input name="age"/><br/>
		<input type="submit" value="Saada"/><br/>
	</form>
</body>
</html>