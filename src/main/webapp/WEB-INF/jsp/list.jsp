<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>
<body>
	<a href="add">Lisa isik</a>
	<br/>
	<br/>
	<c:forEach var="person" items="${requestScope.persons}">
		<c:out value="${person.name} (${person.age})"></c:out>
		<a href="list?cmd=delete&id=${person.id}">Kustuta</a>
		<br/>
	</c:forEach>
</body>
</html>