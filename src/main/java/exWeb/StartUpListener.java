package exWeb;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import dao.SetupDao;

@WebListener
public class StartUpListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent arg0)  { 
    	System.out.println("Started up");
    	new SetupDao().createSchema();
    }
    
    public void contextDestroyed(ServletContextEvent arg0)  { 
    	System.out.println("Closed");
    }
	
}
