package exWeb;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PersonDao;

@WebServlet("/list")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PersonDao dao = new PersonDao();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if("delete".equals(request.getParameter("cmd"))){
			Long id = Long.parseLong(request.getParameter("id"));
			dao.deletePerson(id);
		}
		
		request.setAttribute("persons", getPersonsList());

		request.getRequestDispatcher("WEB-INF/jsp/list.jsp").forward(request, response);

	}

	private List<Person> getPersonsList() {
		return dao.getPersons();
	}

}
