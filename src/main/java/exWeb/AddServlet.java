package exWeb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PersonDao;

@WebServlet("/add")
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PersonDao dao = new PersonDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/form.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name = request.getParameter("name");
		String ageAsString = request.getParameter("age");

		// valideerimine
		List<String> errorsList = validate(name, ageAsString);

		if (!errorsList.isEmpty()) {
			request.setAttribute("errors", errorsList);
			request.getRequestDispatcher("WEB-INF/jsp/form.jsp").forward(request, response);
			return;
		}

		dao.addPerson(new Person(name, Integer.parseInt(ageAsString)));

		response.sendRedirect("list");
	}

	private List<String> validate(String name, String ageAsString) {
		List<String> errorsList = new ArrayList<>();
		if ("".equals(name)) {
			errorsList.add("Sisesta nimi!");
		}
		// ^: algusest, \\d: ja peab olema number (digit), +: numbreid võib mitu
		// olla, $: string peab ära lõppema
		if (ageAsString != null && !ageAsString.matches("^\\d+$")) {
			errorsList.add("Vanus peab olema positiivne number");
		} 
		
		return errorsList;
	}

}
