package dao;

public abstract class AbstractDao {

    protected static final String DB_URL = "jdbc:hsqldb:mem:db1";

    static {
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
