package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import util.FileUtil;

public class SetupDao extends AbstractDao {

	public void createSchema() {
		String statements = FileUtil.readFileFromClasspath("schema.sql");

		for (String statement : statements.split(";")) {
			if (statement.matches("\\s*")) {
				continue;
			}
			executeUpdate(statement);
		}
	}

	private static void executeUpdate(String sql) {
		try (Connection conn = DriverManager.getConnection(DB_URL); Statement stmt = conn.createStatement()) {
			System.out.println("executing: " + sql);
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
