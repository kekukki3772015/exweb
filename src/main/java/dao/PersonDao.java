package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import exWeb.Person;

public class PersonDao extends AbstractDao {

	public List<Person> getPersons() {
		List<Person> persons = new ArrayList<>();

		try (Connection conn = DriverManager.getConnection(DB_URL); Statement stmt = conn.createStatement()) {

			// päring mis küsib konkreetse isiku välja
			try (ResultSet r = stmt.executeQuery("SELECT id, name, age FROM PERSON")) {
				while (r.next()) {
					// saame resultSet'i ja sealt korjame need välja ja teeme
					// neist objektid
					Long id = r.getLong(1);
					String name = r.getString(2);
					int age = r.getInt(3);
					persons.add(new Person(id, name, age));
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return persons;
	}

	public void addPerson(Person person) {

		String sql = "insert into person (id, name, age) values (next value for seq1, ?, ?)";

		try (Connection conn = DriverManager.getConnection(DB_URL); PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setString(1, person.getName());
			ps.setInt(2, person.getAge());

			// executeUpdate() puhul parameetrit ei lisata!
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void deletePerson(long id) {
		String sql = "delete from person where id = ?";

		try (Connection conn = DriverManager.getConnection(DB_URL); PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setLong(1, id);
			// executeUpdate() puhul parameetrit ei lisata!
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
